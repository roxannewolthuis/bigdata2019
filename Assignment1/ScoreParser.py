class ScoreParser:
    def __init__(self, q, inputfile, core, startSize, endSize):
        self.q = q
        self.inputfile = inputfile
        self.core = core
        self.startSize = startSize
        self.endSize = endSize
        self.parse_score()

    def parse_score(self):
        """"Test processing function"""
        start = False

        # Open the given input file
        with open(self.inputfile, "r") as input:
            input.seek(self.startSize)
            result = []
            counters = []

            while input.tell() <= self.endSize:

                line = input.readline()
                if line.startswith("+"):
                    start = True

                # If line is found start process
                if start:
                    score = input.readline()
                    scores = [ord(character) - 33 for character in score[:-1]]
                    number_of_scores = len(scores)

                    # create empty list for scores and counter, add 0 for each index, then sum up the phred scores
                    for number in range(number_of_scores):
                        try:
                            if len(result) < len(scores):
                                result[number] = 0
                                counters[number] = 0
                        except IndexError:
                            result.append(0)
                            counters.append(0)
                        result[number] += scores[number]
                        counters[number] += 1

                    # if input.tell becomes bigger then filesize start = false and stop processing
                    if input.tell() >= self.endSize:
                        print("Finished")
                        self.q.put([result, counters])
                        start = False
                    if not start:
                        break

import sys
import argparse as ap


def parse_arguments():
    parser = ap.ArgumentParser(description='Process FastQ file to produce average PHRED scores per base')
    parser.add_argument("fastq")
    parser.add_argument("-n", "--n", dest='cores', type=int, required=True)
    parser.add_argument("-o", "--o", dest='csv')
    args = parser.parse_args()
    return args

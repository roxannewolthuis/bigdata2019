"""
Author: Roxanne Wolthuis
Date: 13-05-2019
Function: Process function
"""

# imports
# red underlines can be ignored, the imports work
from multiprocessing import Process, Queue
from argument_parser import parse_arguments
from ScoreParser import ScoreParser
import os
import csv
import time

class Processing:
    """..."""
    def __init__(self):
        """Constructor function"""
        self.get_args()
        self.args = parse_arguments()
        self.cores = self.args.cores
        self.inputfile = self.args.fastq
        self.csv = self.args.csv
        self.file_chunk_size = 0
        self.meanList = []

    def get_args(self):
        """This function gets the arguments from argument_parser.py"""
        args = parse_arguments()
        if args.csv:
            self.csv = args.csv

    def calc_chunk_size(self):
        """"This function calculates the chunk size for each core"""
        file_stats = os.stat(self.inputfile)
        self.file_chunk_size = int(file_stats.st_size / self.cores)
        print(self.file_chunk_size)

    def make_process(self):
        """This function creates a process for each core"""
        print("making process")
        start_size = 0
        end_size = self.file_chunk_size
        process_list = []
        q = Queue()

        # create process for each core
        for core in range(0, int(self.cores)):
            p = Process(target=ScoreParser, args=(q, self.inputfile, core, start_size, end_size))
            process_list.append(p)
            p.start()
            # create new start and end size
            start_size += self.file_chunk_size
            end_size += self.file_chunk_size

        sums = []
        counters = []
        mean_list = []
        # close each process
        for process in process_list:
            process.join()
            x, y = q.get()
            sums.append(x)
            counters.append(y)
        sum_result = [sum(x) for x in zip(*sums)]
        sum_count = [sum(x) for x in zip(*counters)]

        for i in range(0, len(sum_result)):
            mean = sum_result[i] / sum_count[i]
            mean_list.append(mean)
        self.meanList = mean_list

    def write(self):
        if self.csv:
            with open(self.csv, mode="w") as csv_file:
                csv_writer = csv.writer(csv_file)
                csv_writer.writerow(["Position", "Score"])
                for i, mean in enumerate(self.meanList):
                    csv_writer.writerow([i, mean])


def main():
    """Main function"""
    start = time.time()
    m = Processing()
    m.calc_chunk_size()
    m.make_process()
    m.write()
    end = time.time()
    process_duration = end - start
    print "Processing time: ", process_duration


if __name__ == '__main__':
    main()

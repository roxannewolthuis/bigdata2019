"""
Author: Roxanne Wolthuis
Date: 21-05-2019
Function: Master function
"""

# imports
# red underlines can be ignored, the imports work
import multiprocessing
from multiprocessing.managers import SyncManager
from multiprocessing import Process, Queue, Pool
from argument_parser import parse_arguments
from worker import ScoreParser
import os, csv, time
from random import randint


class Manage:
	"""This is the manage class.
	The manage class will first create a random port.
	Then the class will start a manager and create multiple workers.
	When the workers are finished the result will be collected and written to an output file(if called on the commandline)
	"""

	def __init__(self):
		"""Constructor function"""
		self.get_args()
		self.args = parse_arguments()
		self.cores = self.args.cores
		self.inputfile = self.args.fastq
		self.csv = self.args.csv
		self.file_chunk_size = 0
		self.meanList = []
		self.worker_pc = ["bin100", "bin101", "bin102", "bin103"]
		self.master_pc = "bin100"
		self.worker_path = self.args.location_of_worker

	def make_port(self):
		"""
		This function will create a random port that is used to start the manager
		"""
		# print("current workingdir,", os.getcwd())
		self.port = randint(10000, 20000)
		print(self.port)
		return

	def make_server_manager(self, port, authkey):
		""" Create a manager for the server, listening on the given port.
			Return a manager object with get_job_q and get_result_q methods.
		"""
		job_q = multiprocessing.Queue()
		result_q = multiprocessing.Queue()

		class JobQueueManager(SyncManager):
			pass

		JobQueueManager.register('get_job_q', callable=lambda: job_q)
		JobQueueManager.register('get_result_q', callable=lambda: result_q)

		manager = JobQueueManager(address=("bin100", self.port), authkey=b'test')
		manager.start()
		print("Server started at port {}".format(self.port))
		return manager
	
	def make_workers(self):
		"""
		For each worker pc create a command and add the command to a list of processes
		"""
		self.worker_processes = []
		for item in self.worker_pc:
			command = "ssh {} python3 {} {} -n {} -w {} -p {} -l {}".format(item, self.worker_path, self.inputfile, self.cores, item, self.port, self.worker_path)
			self.worker_processes.append(command)
		return self.worker_processes

	def run_process(self, process):
		"""
		Run the processes that were created in function make_workers
		"""
		print("running process")                                                         
		os.system('{}'.format(process))

	def runserver(self):
		"""
		Start a server calling make_server_manager().
		Calculate chunk size
		Make workers calling make_workers()
		Use pool to run the processes
		Collect the results from workers
		Close the manager
		"""
		manager = self.make_server_manager(self.port, b'test')
		shared_job_q = manager.get_job_q()
		shared_result_q = manager.get_result_q()
		
		# Get and set sizes
		print("Calculating chunk size")
		file_stats = os.stat(self.inputfile)
		start_chunk = 0
		chunk = int(file_stats.st_size / 1000)
		end_chunk = chunk
		
		# Make chunks
		for i in range(0, 1000):
			start_chunk = chunk * i
			end_chunk = (chunk * i) + chunk
			shared_job_q.put([start_chunk, end_chunk])
		
		# Make workers
		self.make_workers()
		pool = Pool(processes=4)                                                        
		pool.map(self.run_process, self.worker_processes)
		
		# Wait for all results		
		print("Collecting results")
		sums = []
		counters = []
		mean_list = []
		count_result = 0
		while count_result <= 1000:
			som, counter = shared_result_q.get()
			sums.append(som)
			counters.append(counter)
			count_result += 1
			if count_result == 1000:
				break

		sum_result = [sum(x) for x in zip(*sums)]
		sum_count = [sum(x) for x in zip(*counters)]

		#shutdown manager
		time.sleep(2)
		manager.shutdown()
		print("Shutting down manager")
	
		for i in range(0, len(sum_result)):
			mean = sum_result[i] / sum_count[i]
			mean_list.append(mean)
		self.meanList = mean_list

	def get_args(self):
		"""This function gets the arguments from argument_parser.py"""
		args = parse_arguments()
		if args.csv:
			self.csv = args.csv

	def write(self):
		"""Write the collected output to a csv file"""
		if self.csv:
			filename = "output/{}".format(self.csv)
			os.makedirs(os.path.dirname(filename), exist_ok=True)

		with open(filename, mode="w") as csv_file:
			csv_writer = csv.writer(csv_file)
			csv_writer.writerow(["Position", "Score"])
			for i, mean in enumerate(self.meanList):
				csv_writer.writerow([i, mean])
		print("Your output is written to: {}/{}".format(os.getcwd(), filename))


def main():
	"""Main function"""
	start = time.time()
	m = Manage()
	m.make_port()
	m.runserver()
	m.get_args()
	m.write()
	end = time.time()
	process_duration = end - start
	print("Processing time: ", process_duration)

if __name__ == '__main__':
	main()

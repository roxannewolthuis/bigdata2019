import sys
import argparse as ap


def parse_arguments():
    parser = ap.ArgumentParser(description='Process FastQ file to produce average PHRED scores per base')
    parser.add_argument("fastq")
    parser.add_argument("-n", "--n", dest='cores', type=int, required=True)
    parser.add_argument("-o", "--o", dest='csv')
    parser.add_argument("-l", "--l", dest='location_of_worker', required=True)
    parser.add_argument("-w", "--w", dest='worker', default=None)
    parser.add_argument("-p", "--p", dest='port', default=None)
    args = parser.parse_args()
    return args

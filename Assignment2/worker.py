from multiprocessing import Process
from multiprocessing.managers import SyncManager
from argument_parser import parse_arguments
import time, os

class ScoreParser:
	"""
	This class will connect a worker with the manager
	and when connected run the scoreparser to parse information back to the master class
	"""
	def __init__(self):
		self.args = parse_arguments()
		self.cores = self.args.cores
		self.inputfile = self.args.fastq
		self.csv = self.args.csv
		self.worker = self.args.worker
		self.worker_pc = ["bin100", "bin101", "bin102", "bin103"]
		self.port = int(self.args.port)

	def make_client_manager(self, ip, port, authkey):
		""" Create a manager for a client. This manager connects to a server on the
			given address and exposes the get_job_q and get_result_q methods for
			accessing the shared queues from the server.
			Return a manager object.
		"""
		print("Try connect to manager")

		class ServerQueueManager(SyncManager):
			pass

		ServerQueueManager.register('get_job_q')
		ServerQueueManager.register('get_result_q')

		manager = ServerQueueManager(address=("bin100", port), authkey=authkey)

		while True:
			try:
				print("connecting")
				manager.connect()
				break
			except ConnectionRefusedError:
				time.sleep(2)

		print("Client connected to {}:{}".format(ip, port))
		return manager

	def runclient(self):
		"""run the client"""
		manager = self.make_client_manager(self.worker, self.port, b'test')
		self.job_q = manager.get_job_q()
		self.result_q = manager.get_result_q()
		self.make_process()

		return

	def make_process(self):
		"""This function creates a process for each worker"""
		print("making workers")
		worker_list = []

		# create process for each core
		for worker in range(0, int(self.cores)):
			p = Process(target=self.parse_score, args=(self.inputfile, self.job_q, self.result_q))
			worker_list.append(p)
			p.start()

		for i in worker_list:
			i.join()
		return

	def parse_score(self, inputfile, job_q, result_q):
		"""
		This function opens the input file and parsers the score while the job is not empty,
		If the job is empty the process should stop
		"""
		startloc = False
		result = []
		counters = []
		with open(self.inputfile, "r") as inputt:

			while not job_q.empty():
				try:
					start, end = self.job_q.get()
					inputt.seek(start)

					while True:
						line = inputt.readline()
						if line.startswith("+"):
							startloc = True

						# If line is found start process
						if startloc:
							score = inputt.readline()
							scores = [ord(character) - 33 for character in score[:-1]]
							number_of_scores = len(scores)

						# create empty list for scores and counter, add 0 for each index, then sum up the phred scores
							for number in range(number_of_scores):
								try:
									if len(result) < len(scores):
										result[number] = 0
										counters[number] = 0
								except IndexError:
									result.append(0)
									counters.append(0)
								result[number] += scores[number]
								counters[number] += 1

							# if input.tell becomes bigger then filesize start = false and stop processing
							if inputt.tell() >= end:
								result_q.put([result, counters])
								time.sleep(0.3)
								break
				except EOFError:
					return 0

def main():
	"""Main function"""
	w = ScoreParser()
	w.runclient()

if __name__ == '__main__':
	main()

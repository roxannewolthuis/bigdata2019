# Bigdata2019
* **Author:**            	Roxanne Wolthuis
* **Student numbers:**      347958
* **Class:**                BFV3
* **Course:**               HPC
* **Study:**                Bio-informatics
* **Institute:**            Institute for Lifescience & Technology
* **Lector:**               Martijn Herber
* **Date:**                 13 - 05 - 2019

## Content of this repository
* **Assignment 1:**
A folder with all files for Assignment 1.

* **Assignment 2:**
A folder with al files for Assignment 2.

* **Readme:**
The readme tells you where to find each file and how the script works.

## Prerequisites
- Python3.5 or newer. Python is the scripting language used to create the program.  
https://www.python.org/downloads/

## Usage

* **First command: **
Before running the script ssh to bin100  
- **Command example:** ```ssh bin100```

* **Second command: **
Here is a command example listed to show how the script can be runned  
- **Command example:**  ```python3 PycharmProjects/bigdata2019/Assignment2/Master.py /commons/Themas/Thema12/HPC/rnaseq.fastq -n 8 -o results_rna.csv -l PycharmProjects/bigdata2019/Assignment2/worker.py```  
First argument is the path to the program(Master.py)  
Second argument is the path to the fastq file  
-n is the number of cores(required)  
-o is a result.csv file(optional)  
-l is the path to the worker file(required)  

## Parameters for the pipeline
All possible parameters for this pipeline are listed below.  
The parameters can be added in a row separated by spaces on the command line

- **-n**
is the number of cores(required)
- **-o**
is a result.csv file(optional)
- **-l**
is a the path to the workerfile file(required)
## Issues
- 
